-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Apr 12, 2018 at 01:04 PM
-- Server version: 5.6.39
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pclibre`
--

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `designation` varchar(15) NOT NULL,
  `description` text NOT NULL,
  `tauxhoraire` double NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `departement` varchar(30) NOT NULL,
  `idproprio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `article`
--

INSERT INTO `article` (`id`, `designation`, `description`, `tauxhoraire`, `image`, `departement`, `idproprio`) VALUES
(24, 'Pc aesus electr', 'Pc portable pour développeurs 8 Go de Ram', 21, '05_41_40ammacbookheader.jpg', 'Paris', 1),
(25, 'Pc aesus extra ', 'Pc portable pour tâches bureutiques, léger et facile à transporter', 1, '05_43_20ampcportable.jpg', 'Paris', 1),
(26, 'Pc Gamer Hp', 'Pc pour gamers de marque HP, design hard rock, 16 Go de RAM, processeur Graphique AMD', 2, '05_45_38amabstract-background-design_1297-87.jpg', 'Creteil', 1),
(27, 'Asus ZENBOOK', 'Un ordinateur portable de 13 pouces avec un écran de 14 pouces', 1, '06_27_22am06_25_18amaesus_zenbook.jpg', 'Paris', 1),
(28, 'Asus ZENBOOK', 'The world’s most prestigious laptop with unprecedented performance\r\n\r\nTo say that ZenBook 3 is a new generation of ZenBook is a huge understatement — it’s ZenBook reimagined for the next era of mobile computing. Every single precision component and every elegantly crafted detail has been completely re-engineered and redesigned to make ZenBook 3 the most sophisticated ZenBook yet. It’s lighter, thinner, stronger, impossibly powerful — and utterly beautiful. Simply, it’s the world’s most incredible laptop. Say hello to ZenBook 3.', 21, '06_42_40amaesus_zenbook.jpg', 'Paris', 1);

-- --------------------------------------------------------

--
-- Table structure for table `article_categorise`
--

CREATE TABLE `article_categorise` (
  `idarticle` int(11) NOT NULL,
  `idcategorie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `code` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `changement_etat_demande`
--

CREATE TABLE `changement_etat_demande` (
  `iddemande` int(11) NOT NULL,
  `idetat` int(11) NOT NULL,
  `datechangement` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `demande`
--

CREATE TABLE `demande` (
  `id` int(11) NOT NULL,
  `datecreation` date NOT NULL,
  `iddemandeur` int(11) NOT NULL,
  `idarticle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `etat`
--

CREATE TABLE `etat` (
  `id` int(11) NOT NULL,
  `libelle` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `name` varchar(15) NOT NULL,
  `firstname` varchar(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `isadmin` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `firstname`, `username`, `password`, `isadmin`) VALUES
(1, 'az@az', 'SIMO', 'philippe', 'philippe', 'philippe', 0),
(5, 'az@az.com', 'SIMO', 'philippe', 'fabrice', 'philippe', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_idproprio` (`idproprio`);

--
-- Indexes for table `article_categorise`
--
ALTER TABLE `article_categorise`
  ADD PRIMARY KEY (`idarticle`,`idcategorie`),
  ADD KEY `article_categorise_ibfk_idcategorie` (`idcategorie`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `changement_etat_demande`
--
ALTER TABLE `changement_etat_demande`
  ADD PRIMARY KEY (`iddemande`,`idetat`),
  ADD KEY `idetat` (`idetat`);

--
-- Indexes for table `demande`
--
ALTER TABLE `demande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `demande_ibfk_id_demandeur` (`iddemandeur`),
  ADD KEY `demande_ibfk_id_article` (`idarticle`);

--
-- Indexes for table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `article`
--
ALTER TABLE `article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `demande`
--
ALTER TABLE `demande`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `etat`
--
ALTER TABLE `etat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_idproprio` FOREIGN KEY (`idproprio`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article_categorise`
--
ALTER TABLE `article_categorise`
  ADD CONSTRAINT `fk_id_article` FOREIGN KEY (`idarticle`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_categorie` FOREIGN KEY (`idcategorie`) REFERENCES `categorie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `changement_etat_demande`
--
ALTER TABLE `changement_etat_demande`
  ADD CONSTRAINT `changement_etat_demande_fk_iddemande` FOREIGN KEY (`iddemande`) REFERENCES `demande` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `changement_etat_demande_fk_idetat` FOREIGN KEY (`idetat`) REFERENCES `etat` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `demande`
--
ALTER TABLE `demande`
  ADD CONSTRAINT `deamnde_fk_id_article` FOREIGN KEY (`idarticle`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_id_demandeur` FOREIGN KEY (`iddemandeur`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
