<?php
/**
 * Created by PhpStorm.
 * User: philippe
 * Date: 02/04/2018
 * Time: 11:36
 */

require_once 'connection.php';

class User
{

    private $id;
    private $email;
    private $name;
    private $firstname;
    private $username;
    private $password;
    private $isadmin;

    /**
     * User constructor.
     * @param $email
     * @param $name
     * @param $firstname
     * @param $username
     * @param $password
     * @param $isadmin
     */
    public function __construct($email, $name, $firstname, $username, $password, $isadmin)
    {

        $this->email = $email;
        $this->name = $name;
        $this->firstname = $firstname;
        $this->username = $username;
        $this->password = $password;
        $this->isadmin = $isadmin;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return User
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param mixed $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIsadmin()
    {
        return $this->isadmin;
    }

    /**
     * @param mixed $isadmin
     * @return User
     */
    public function setIsadmin($isadmin)
    {
        $this->isadmin = $isadmin;
        return $this;
    }

    public function connectUser()
    {

        $mysqli = getDbConnection();
        $sql = "SELECT * FROM user WHERE (username='" . $this->username . "' OR email='".$this->username."') AND password='" . $this->password . "'";
        $result = $mysqli->query($sql);
        if ($result->num_rows > 0) {
            $aUser = $result->fetch_object();
            $this->username = $aUser->username;
            $this->password = $aUser->password;
            $this->isadmin = $aUser->isadmin;
            $this->name = $aUser->name;
            $this->firstname = $aUser->firstname;
            $this->email = $aUser->email;
            $this->id = $aUser->id;
        }

        $mysqli->close();
        return $result->num_rows;

    }

    public function registerUser()
    {
        $mysqli = getDbConnection();
        $sql = "INSERT INTO `user` (`email`, `name`, `firstname`, `username`, `password`, `isadmin`) VALUES ('" . $this->email . "','" . $this->name . "','" . $this->firstname . "','" . $this->username . "','" . $this->password . "','" . $this->isadmin . "')";

        if (!$mysqli->query($sql)) {
            printf("Erreur: %s\n", $mysqli->error);
            return false;
        }
        return true;

    }

    public function getUserById()
    {
        $mysqli = getDbConnection();
        $sql = "SELECT * FROM user WHERE username='" . $this->username . "' OR email='" . $this->email . "'";
        $result = $mysqli->query($sql);
        $mysqli->close();
        return $result->num_rows;


    }


}