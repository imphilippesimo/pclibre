<?php
/**
 * Created by PhpStorm.
 * User: philippe
 * Date: 11/04/2018
 * Time: 15:18
 */
require_once 'connection.php';
require_once 'Article.php';

function getAllArticles()
{

    $mysqli = getDbConnection();
    $sql = "SELECT * FROM article";
    $result = $mysqli->query($sql);
    $articles = array();
    while ($article = $result->fetch_object()) {
        $articles[] = $article;
    }
    return $articles;


}