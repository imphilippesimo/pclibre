<?php
/**
 * Created by PhpStorm.
 * User: philippe
 * Date: 02/04/2018
 * Time: 11:59
 */

require_once 'User.php';
session_start();

$message = '';

if (isset($_POST['username']) AND isset($_POST['password']) AND isset($_POST['email']) AND isset($_POST['name']) AND isset($_POST['firstname'])) {
    $user = new User($_POST['email'], $_POST['name'], $_POST['firstname'], $_POST['username'], $_POST['password'], '');
    if ($user->getUserById() <= 0) {
        //enregistrement de l'utilisateur
        if ($user->registerUser()) {

            $message = "Compte créé avec succès, vous pouvez vous connecter!";

            /* Redirection vers la page de connexion */
            header("Location: http://localhost/pclibre/scripts/php/loginOrSubscribe.php?login=1&message=" . $message . "&messageClass=success");
            /* Arreter toute execution suivante de code */
            exit;

        }


    } else {
        $message = "Un utilisateur existe déjà avec ce pseudonyme ou email!!!";

        /* Redirection vers la page de connexion  avec message erreur*/
        header("Location: http://localhost/pclibre/scripts/php/loginOrSubscribe.php?message=" . $message . "&messageClass=failure");
        /* Arreter toute execution suivante de code */
    }


}



