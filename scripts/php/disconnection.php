<?php
/**
 * Created by PhpStorm.
 * User: philippe
 * Date: 02/04/2018
 * Time: 18:18
 */
session_start();
if (isset($_SESSION["user"]) AND !empty($_SESSION["user"])) {
    unset($_SESSION["user"]);

    /* Redirection vers la page d'accueil*/
    header("Location: http://localhost/pclibre");
    /* Arreter toute execution suivante de code */
    exit;

}


