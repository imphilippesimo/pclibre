<?php
/**
 * Created by PhpStorm.
 * User: philippe
 * Date: 02/04/2018
 * Time: 11:59
 */

require_once 'User.php';
session_start();

if (isset($_POST['username']) AND isset($_POST['password'])) {
    $user = new User('', '', '', $_POST['username'], $_POST['password'], '');
    if ($user->connectUser() > 0) {
        //sauvegarde du nom de l'utilisateur en session
        $_SESSION["user"] = serialize($user);

        //echo $_SESSION["user"];
        /* Redirection vers la page d'accueil*/
        header("Location: http://localhost/pclibre");
        /* Arreter toute execution suivante de code */
        exit;

    } else {
        $message = "Identifiant ou mot de passe incorrect!";
        /* Redirection vers la page de connexion */
        header("Location: http://localhost/pclibre/scripts/php/loginOrSubscribe.php?login=1&message=".$message);
        /* Arreter toute execution suivante de code */
        exit;
    }


}



