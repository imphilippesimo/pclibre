<!DOCTYPE html>
<html>
<head>
    <link href="../css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="login-bg">

    <div class="login-header">
        <h1>PC Libre</h1>
        <h2>Economiser à travers le partage de PC</h2>

        <form action="http://localhost/pclibre">
            <button type="submit">Voir les articles</button>
        </form>

    </div>


    <?php
    include_once 'message.php';
    if (isset($_GET['login'])) {

        if ($_GET['login'] == 1) {

            include 'login.php';
        } else {

            include 'subscribe.php';
        }

    } else {
        include 'subscribe.php';
    }
    ?>
    <?php
    include 'footer.php';
    ?>
</div>
</body>

</html>
