<?php
/**
 * Created by PhpStorm.
 * User: philippe
 * Date: 02/04/2018
 * Time: 12:06
 */


function getDbConnection()
{
    $host = "localhost";
    $user = "root";
    $mdp = "philippe";
    $bdd = "pclibre";
    $port = "3307";
    $mysqli = new mysqli($host, $user, $mdp, $bdd,$port);
    if ($mysqli->connect_errno) {
        die("<p> Failed to connect to MySQL: (" . $mysqli->connect_errno . ") " . $mysqli->connect_error. " </p>");
    }
    return $mysqli;
}