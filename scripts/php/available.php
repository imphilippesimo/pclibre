<?php
require_once 'Article.php';
require_once 'ArticleServices.php';

$articles = getAllArticles();
if (count($articles) == 0)
    echo "<h2> Désolé, il n'existe aucun article pour le moment</h2>";
else {
    echo "<h2> Articles disponibles</h2>";
    foreach ($articles as $article) {
        $imagePath = join(DIRECTORY_SEPARATOR, array("images", $article->image));


        echo "<div class=\"card\">
                <h2>" . $article->designation . "</h2>
                <h5>Taux horaire: " . $article->tauxhoraire . " &euro;</h5>                
                 <div class=\"details\">
                    <p>.$article->description.</p>    
                </div>
           
    
                <div class=\"image\">
                    <img src=" . $imagePath . " alt=\"Image de l'article\">
                </div>    
               
              </div>";
    }
}





