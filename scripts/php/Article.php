<?php
/**
 * Created by PhpStorm.
 * User: philippe
 * Date: 08/04/2018
 * Time: 17:33
 */

require_once 'connection.php';

class Article
{
    private $id;
    private $designation;
    private $description;
    private $tauxhoraire;
    private $image;
    private $departement;
    private $idproprio;


    /**
     * article constructor.
     * @param $designation
     * @param $description
     * @param $tauxhoraire
     * @param $image
     * @param $departement
     * @param $idproprio
     */
    public function __construct($designation, $description, $tauxhoraire, $image, $departement, $idproprio)
    {
        $this->designation = $designation;
        $this->description = $description;
        $this->tauxhoraire = $tauxhoraire;
        $this->image = $image;
        $this->departement = $departement;
        $this->idproprio = $idproprio;
    }


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Article
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDesignation()
    {
        return $this->designation;
    }

    /**
     * @param mixed $designation
     * @return Article
     */
    public function setDesignation($designation)
    {
        $this->designation = $designation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     * @return Article
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getTauxhoraire()
    {
        return $this->tauxhoraire;
    }

    /**
     * @param mixed $tauxhoraire
     * @return Article
     */
    public function setTauxhoraire($tauxhoraire)
    {
        $this->tauxhoraire = $tauxhoraire;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return Article
     */
    public function setImage($image)
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * @param mixed $departement
     * @return Article
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIdproprio()
    {
        return $this->idproprio;
    }

    /**
     * @param mixed $idproprio
     * @return Article
     */
    public function setIdproprio($idproprio)
    {
        $this->idproprio = $idproprio;
        return $this;
    }

    public function registerArticle()
    {

        $mysqli = getDbConnection();
        $sql = "INSERT INTO `article` (`designation`,`description`,`tauxhoraire`,`image`, `departement`,`idproprio`) VALUES ('" . $this->designation . "','" . $this->description . "','" . $this->tauxhoraire . "','" . $this->image . "','" . $this->departement . "','" . $this->idproprio . "')";

        if (!$mysqli->query($sql)) {
            printf("Erreur: %s\n", $mysqli->error);
            return false;
        }
        return true;


    }




}