<?php
/**
 * Created by PhpStorm.
 * User: philippe
 * Date: 08/04/2018
 * Time: 17:15
 */

session_start();
require_once 'User.php';
require_once 'Article.php';

$uploadOk = 1;

// la taille du fichier ne doit pas dépasser 5 Mo
if ($_FILES["image"]["size"] > 5000000) {
    echo "Fichier trop lourd";
    $uploadOk = 0;
}


if (isset($_POST["designation"]) AND isset($_POST["description"]) AND isset($_POST["tauxhoraire"]) AND isset($_POST["departement"]) AND isset($_SESSION["user"])) {



    //ajouter le temps de l'upload au nom de l'image pour éviter des doublons
    $imageName = date("h_i_sa").basename($_FILES["image"]["name"]);

    //construire le chemin d'upload
    $target_dir = "../../images/";
    $target_file = join(DIRECTORY_SEPARATOR,array($target_dir,$imageName));

    //récuperation de l'extension du fichier
    $imageFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));



    // vérifier s'il s'agit réellement d'une image
    //if (isset($_POST["submit"])) {
    $check = getimagesize($_FILES["image"]["tmp_name"]);
    if ($check !== false) {
        //le fichier est une image
        $uploadOk = 1;
    } else {
        //le fichier n'est pas une image
        $uploadOk = 0;
    }
    // }


// Vérification du type de fichiers
    if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif") {
        echo "Désolé, seuls les fichiers de type jpg, png, jpeg ou gif sont autorisés <br/>";
        $uploadOk = 0;
    }

    if ($uploadOk == 0) {
        echo "Désolé votre fichir ne peut être uploadé";

    } else {
        //recuperation l'utilisateur en session
        $user = unserialize($_SESSION["user"]);

        move_uploaded_file($_FILES["image"]["tmp_name"], $target_file);

        //construction d'un article
        $article = new Article($_POST["designation"], $_POST["description"], $_POST["tauxhoraire"], $imageName, $_POST["departement"], $user->getId());
        if ($article->registerArticle()) {
            $message = "Article proposé avec succès!";
            /* Redirection vers la page d'accueil*/
            header("Location: http://localhost/pclibre?message=" . $message . "&messageClass=success");
            /* Arreter toute execution suivante de code */
            exit;

        }


    }


}




