<!DOCTYPE html>
<html>
<head>
    <link href="scripts/css/style.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<div class="bg">

    <div class="header">
        <h1>PC Libre</h1>

        <?php
        session_start();
        require_once 'scripts/php/User.php';

        if (isset($_SESSION["user"]) AND !empty($_SESSION["user"])) {
            $user = unserialize($_SESSION["user"]);
            $name = $user->getName();
            $connectionText = "Déconnexion";
            $connectAction = "scripts/php/disconnection.php";

            $firstname = $user->getFirstname();
            echo "<h2>" . $firstname . "  " . $name . "</h2>";

        } else {
            $connectionText = "Connexion";
            $connectAction = "scripts/php/loginOrSubscribe.php?login=1";
        }
        ?>

        <h2>Economisez à travers le partage de PC</h2>

    </div>

    <div class="topnav">
        <a href="index.php?action=available">Articles</a>
        <a href="index.php?action=newArticle">Proposer une offre</a>
        <?php
        echo '  <a href="' . $connectAction . '" style="float:right">' . $connectionText . '</a>';
        ?>


    </div>

    <div class="row">
        <div class="leftcolumn">
            <?php
            if (isset($_GET['action'])) {
                include_once 'scripts/php/'.$_GET['action'].'.php';
            } else {
                include_once 'scripts/php/available.php';
            }
            ?>
        </div>

        <div class="rightcolumn">
            <div class="card">
                <h2>A Propos</h2>
                <div class="image">Image</div>
                <p>Pc Libre est une plateforme pour la partage d'ordianteurs</p>
            </div>
            <div class="card">
                <h3>Articles populaires</h3>
                <div class="image"><p>Image</p></div>
                <div class="image"><p>Image</p></div>
                <div class="image"><p>Image</p></div>
            </div>
            <div class="card">
                <h3>Suivez nous sur</h3>
                <p>Réseaux sociaux</p>
            </div>
        </div>
    </div>

    <?php
    include 'scripts/php/footer.php';
    ?>
</div>
</body>

</html>
