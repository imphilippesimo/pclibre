-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Apr 02, 2018 at 08:30 AM
-- Server version: 5.6.39
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pclibre`
--
CREATE DATABASE IF NOT EXISTS `pclibre` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `pclibre`;

-- --------------------------------------------------------

--
-- Table structure for table `article`
--

CREATE TABLE `article` (
  `id` int(11) NOT NULL,
  `designation` varchar(15) NOT NULL,
  `description` text NOT NULL,
  `tauxhoraire` double NOT NULL,
  `image` blob,
  `departement` varchar(30) NOT NULL,
  `idproprio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `article_categorise`
--

CREATE TABLE `article_categorise` (
  `idarticle` int(11) NOT NULL,
  `idcategorie` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `id` int(11) NOT NULL,
  `code` varchar(30) NOT NULL,
  `nom` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `changement_etat_demande`
--

CREATE TABLE `changement_etat_demande` (
  `iddemande` int(11) NOT NULL,
  `idetat` int(11) NOT NULL,
  `datechangement` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `demande`
--

CREATE TABLE `demande` (
  `id` int(11) NOT NULL,
  `datecreation` date NOT NULL,
  `iddemandeur` int(11) NOT NULL,
  `idarticle` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `etat`
--

CREATE TABLE `etat` (
  `id` int(11) NOT NULL,
  `libelle` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------


--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(30) NOT NULL,
  `name` varchar(15) NOT NULL,
  `firstname` varchar(15) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL,
  `isadmin` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `name`, `firstname`, `username`, `password`, `isadmin`) VALUES
(1, 'az@az', 'SIMO', 'philippe', 'philippe', 'philippe', 0),
(5, 'az@az.com', 'SIMO', 'philippe', 'fabrice', 'philippe', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `article`
--
ALTER TABLE `article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_idproprio` (`idproprio`);

--
-- Indexes for table `article_categorise`
--
ALTER TABLE `article_categorise`
  ADD PRIMARY KEY (`idarticle`,`idcategorie`),
  ADD KEY `article_categorise_ibfk_idcategorie` (`idcategorie`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `changement_etat_demande`
--
ALTER TABLE `changement_etat_demande`
  ADD PRIMARY KEY (`iddemande`,`idetat`),
  ADD KEY `idetat` (`idetat`);

--
-- Indexes for table `demande`
--
ALTER TABLE `demande`
  ADD PRIMARY KEY (`id`),
  ADD KEY `demande_ibfk_id_demandeur` (`iddemandeur`),
  ADD KEY `demande_ibfk_id_article` (`idarticle`);

--
-- Indexes for table `etat`
--
ALTER TABLE `etat`
  ADD PRIMARY KEY (`id`);


--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);



--
-- Constraints for dumped tables
--

--
-- Constraints for table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `fk_idproprio` FOREIGN KEY (`idproprio`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `article_categorise`
--
ALTER TABLE `article_categorise`
  ADD CONSTRAINT `article_categorise_ibfk_idarticle` FOREIGN KEY (`idarticle`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `article_categorise_ibfk_idcategorie` FOREIGN KEY (`idcategorie`) REFERENCES `categorie` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `changement_etat_demande`
--
ALTER TABLE `changement_etat_demande`
  ADD CONSTRAINT `changement_etat_demande_ibfk_1` FOREIGN KEY (`iddemande`) REFERENCES `demande` (`id`),
  ADD CONSTRAINT `changement_etat_demande_ibfk_2` FOREIGN KEY (`idetat`) REFERENCES `etat` (`id`);

--
-- Constraints for table `demande`
--
ALTER TABLE `demande`
  ADD CONSTRAINT `demande_ibfk_id_article` FOREIGN KEY (`idarticle`) REFERENCES `article` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `demande_ibfk_id_demandeur` FOREIGN KEY (`iddemandeur`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
